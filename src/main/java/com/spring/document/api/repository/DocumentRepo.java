package com.spring.document.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.spring.document.api.model.DocumentModel;

public interface DocumentRepo extends MongoRepository<DocumentModel, Integer> {

}
