package com.spring.document.api.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.spring.document.api.model.DocumentModel;
import com.spring.document.api.repository.DocumentRepo;

@Service
public class ServiceImpl {

  @Autowired
  
  private DocumentRepo docRepo;


  private final Path root = Paths.get("C:\\Users\\hp\\Desktop\\uploads");

  public String save(MultipartFile file,DocumentModel docModel) {
    try {
      Files.copy(file.getInputStream(), root.resolve(file.getOriginalFilename()));
    } catch (IOException e) {
      e.printStackTrace();
    }
    String url= root +"\\"+ file.getOriginalFilename();
    docModel.setDocumentpath(url);
    docRepo.save(docModel);
    return url;
  }

}
