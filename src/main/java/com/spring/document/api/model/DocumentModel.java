package com.spring.document.api.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Document(collection = "Document")

public class DocumentModel {

  
  private String documentpath;

}
