package com.spring.document.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.spring.document.api.model.DocumentModel;
import com.spring.document.api.service.ServiceImpl;

@RestController
@RequestMapping("/document")
public class DocumentController {

  @Autowired
  private ServiceImpl impl;

  @PostMapping
  public String uploadFile(@RequestParam(value = "file", required = true) MultipartFile file,
    DocumentModel docModel) {

    return impl.save(file, docModel);
  }
}